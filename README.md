# ubuntu_btrfs role

Setup btrfs on Ubuntu.

- Makes sure that the btrfs root is _clean_, i.e. that all content is
  stored in subvolumes that can be snapshotted.

- Makes sure there is no regular swapfile, which does not work good in
  btrfs, and offers to setup one that _will_ work on btrfs in a
  dedicated subvolume with appropriate attributes set.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## Dependencies


_This role does not depend on other Ansible roles._


## Role Variables

* `ubuntu_btrfs_swap_size_mb`

    Create a swap file of this size (in MiB)

    If this variable is set to 0, no swap is setup. Use the variable
    `ansible_memtotal_mb` to relate the value to the amount of
    installed RAM.

    Default value: 0

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ubuntu_btrfs
```
